import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import 'react-native-gesture-handler'
import NongkrongYuk from './NongkrongApp/index';
// Import the functions you need from the SDKs you need
import { getApps, initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBgEhHaTyzAbcpu01F77_OKpv6wePFgaUU",
  authDomain: "nongkrongyukapp.firebaseapp.com",
  projectId: "nongkrongyukapp",
  storageBucket: "nongkrongyukapp.appspot.com",
  messagingSenderId: "641261892843",
  appId: "1:641261892843:web:1dc1ba96005b5cb6eb7aae",
  measurementId: "G-5VQP9N1YPV"
};

// // Initialize Firebase
if (!getApps.length) {
    initializeApp(firebaseConfig);
}
// const analytics = getAnalytics(app);


export default function App() {
  return (
    <>
    <StatusBar translucent={false}/>
    <NongkrongYuk/>
    </>       
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});