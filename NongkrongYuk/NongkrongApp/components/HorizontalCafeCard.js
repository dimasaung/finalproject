import { TouchableOpacity, View, Text,Image } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';

const HorizontalCafeCard = ({containerStyle, imageStyle, item, onPress, navigation}) => {
        return (
            <TouchableOpacity 
                style={{
                    flexDirection: 'row',
                    borderRadius: 10,
                    backgroundColor: '#FFF9F3',
                    ...containerStyle
                }}
                >
                {/* {Image} */}
                <Image 
                    source={item.image}
                    style={imageStyle} />

                {/* {Info} */}
                <View
                    style={{
                        flex: 1,
                        left: 30,
                    }}
                >
                    <Text style={{fontSize: 18}}>{item.title} {'\n'}</Text> 
                    <Text style={{fontSize: 14}}>{item.alamat}</Text> 
                    <Text style={{fontSize: 14}}>{item.jam}</Text> 
                    <Text style={{fontSize: 14}}>{item.harga}</Text> 
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        position: 'absolute',
                        top: 10,
                        right: 10,
                    }}
                >
                    <FontAwesome name="heart" size={22} color="red" />


                </View>
            </TouchableOpacity>

        )
    }
export default HorizontalCafeCard