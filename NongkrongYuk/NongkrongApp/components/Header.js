import React from 'react'
import { View, Text } from 'react-native'

const Header = ({containerStyle, title, rightComponent}) => {
    return (
        <View
        style={{
            flexDirection: 'row',
            ...containerStyle
        }}>
            {/* {Left} */}

            {/* {Title} */}
        <View style={{
            flex:1
        }}>
            <Text style={{fontSize:18, color: "#FFFFFF", top:-5}}>Welcome Back,</Text>
            <Text style={{fontSize:18, fontWeight:'bold', color: "#FFFFFF"}}>Username </Text>
        </View>
            {/* {Right} */}
            {rightComponent}
        </View>
    )
}
export default Header