import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import 'react-native-gesture-handler'

export default function Sukses({navigation}) {
  return (
      <View style={styles.container}>
        <Image
                style={{ height: 238, width: 265, top : -40 }}
                source={require("../assets/Sukses.png")}
        />
        <Text style ={styles.ucapan} >REGISTRASI BERHASIL</Text>
        <TouchableOpacity style={styles.buttonLanjutkan}>
                <Text style={styles.textLanjutkan} onPress={()=>navigation.navigate("Login")}>Nongkrong Yuk</Text>
            </TouchableOpacity>
      </View>
  )
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FF7F50",
  },
ucapan: {
    fontSize: 20,
    color: "#FFFFFF"
},
buttonLanjutkan: {
    height: 50,
    width: 150,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    backgroundColor: "#22C7A9",
    bottom: -30,
},
textLanjutkan: {
    fontSize: 18,
    color: "#FFFFFF",
}    
});