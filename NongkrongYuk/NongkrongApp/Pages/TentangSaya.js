import React from 'react';
import { Button, StyleSheet, Text, TextInput, View, Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function TentangSaya() {
    return (
        <View style={styles.container}>
            <Text style={styles.TentangSaya}>Tentang Saya</Text>
            <MaterialCommunityIcons style={styles.avatar}
               name="face-profile" size={90} color="black" />
            <Text style={styles.Nama}>Dimas Agung Prasetyo</Text>            
            <Text style={styles.Label2}>Portofolio</Text>     
            <Text style={styles.Label3}>Project dan karya yang pernah saya kerjakan</Text>
            <View style={styles.icon}>
               <MaterialCommunityIcons name="gitlab" size={40} color="black" />
               <Text style={styles.Label1}>@dimasaung</Text>
            </View>
            <Text style={styles.Label2}>Kontak</Text>        
            <Text style={styles.Label3}>Temukan dan terhubung dengan saya melalui</Text>           
            <Image 
                style ={styles.Logo1}
                source={require("../assets/Gmail.png")}/>
                <Text style={styles.Label4}>dimasaung@gmail.com</Text>            
            <Image 
                style ={styles.Logo2}
                source={require("../assets/WhatsApp-icon-PNG.png")}/>
                <Text style={styles.Label5}>081223244279</Text>
            <Image 
                style ={styles.Logo3}
                source={require("../assets/Instagram.png")}/>
                <Text style={styles.Label6}>@dimasaprasetyo</Text>
          </View>          
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 20,
      backgroundColor: 'white',
    },
    TentangSaya: {
      fontSize: 30,
      fontWeight: "bold",
      color: "#4f4f4f",
      marginBottom: 5,
      marginVertical: 112,
      top: -100,
    },
    Nama: {
      fontSize: 18,
      fontWeight: "bold",
      color: "#4f4f4f",
      marginBottom: 5,
      left: 120,
      top: -150,
    },
    Label: {
        fontSize: 22,
        fontWeight: "bold",
        color: "#4f4f4f",
        marginBottom: 5,
        top : -100,
    },
    Label1: {
      fontSize: 14,
      color: "#4f4f4f",
      marginBottom: 5,
      top: -5,       
    },
    Label2: {
        fontSize: 18,
        color: "#4f4f4f",
        marginBottom: 5,
        top: -100, 
        left: 10,      
    },
    Label3: {
      fontSize: 10,
      color: "#4f4f4f",
      marginBottom: 5,
      left: 10,
      top: -100
    },
    Label4: {
      fontSize: 10,
      color: "#4f4f4f",
      marginBottom: 5,
      left: 15,
      top: -85,
    },
    Label5: {
      fontSize: 10,
      color: "#4f4f4f",
      marginBottom: 5,
      left: 130,
      top: -133,
    },
    Label6: {
      fontSize: 10,
      color: "#4f4f4f",
      marginBottom: 5,
      left: 210,
      top: -183,
    },
    icon :{
      alignItems: "center",  
      top: -100, 
    },
    avatar: {
      left : 10,
      top: -90,
    },
    Logo1:{
      width: 30,
      height: 30,
      left: 50,
      top: -85,
    },
    Logo2:{
      width: 30,
      height: 30,
      left: 150,
      top: -135,
    },
    Logo3:{
      width: 30,
      height: 30,
      left: 235,
      top: -185,
    }
});