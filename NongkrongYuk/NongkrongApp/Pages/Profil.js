import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, Button} from 'react-native'
import { getAuth, signOut} from "firebase/auth";

export default function Profil({navigation}) {
    const onSignedOut = () => {
    const auth=getAuth()
    signOut(auth).then(()=>{
        console.log(`Logout Berhasil`);
        navigation.navigate("Login")
    // Sign-Out successful
    }).catch((error) => {
    // An error happened
    })
}
    
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 25}}>Profil  Pengguna</Text>
            <Button onPress={onSignedOut} title= "Keluar"/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    }
})
