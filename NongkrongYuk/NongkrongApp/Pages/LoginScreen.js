import {useNavigation} from "@react-navigation/native"
import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button, TouchableOpacity } from "react-native";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

export default function Login({navigation}) {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [isError, setIsError] = useState(false)

    //Masuk
    const onSignedIn = () => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Masuk
        const user = userCredential.user;
        console.log(`Login as : ${user.email}`);
        navigation.navigate('MyDrawer', {
          screen: 'App',
          params : {
            screen : "AboutScreen",
        },
        });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert(errorCode);
        console.log(errorCode, errorMessage);
      });
  }

    return (
        <View style={styles.container}>
            <Image
                style={{ height: 150, width: 200, top : -50 }}
                source={require("../assets/Welcome.png")}
            />
            <Text style={{ fontSize: 18, fontWeight: "bold", left: -120, top : -10 }}>Masuk</Text>
            <View>
            <TextInput
              style={{
                paddingVertical: 10,
                borderRadius: 20,
                width: 300,
                height: 63,
                marginBottom: 10,
                paddingHorizontal: 10,
                backgroundColor: '#F3F3F3',
              }}
              placeholder="Email"
              value={email}
              onChangeText={(value)=>setEmail(value)}
            />
            <TextInput
              style={{
                paddingVertical: 10,
                borderRadius: 20,
                width: 300,
                height: 63,
                marginBottom: 10,
                paddingHorizontal: 10,
                backgroundColor: '#F3F3F3',
              }}
              secureTextEntry
              placeholder="Password"
              value={password}
              onChangeText={(value)=>setPassword(value)}
            />
            <TouchableOpacity>
                <Text style={styles.forgot} onPress={()=>navigation.navigate("Forgot")}>Forgot password?</Text>
            </TouchableOpacity>       
            <TouchableOpacity style={styles.buttonMasuk}>
                <Text style={styles.textMasuk} onPress={onSignedIn}>Masuk</Text>
            </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#FF7F50"
    },
    forgot: {
        fontSize: 12,
        color: "#444444",
        marginBottom: 5,
        alignSelf: "flex-end",
    },
    buttonMasuk: {
        height: 50,
        width: 150,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: "#22C7A9",
        bottom: -40,
    },
    textMasuk: {
        fontSize: 18,
        color: "black",
    },
})
    
