import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { useNavigation } from "@react-navigation/native"
import React from "react";

export default function Welcome({navigation}) {
    return (
        <View style={styles.container}> 
            <Image
                style={{ height: 150, width: 200, top : -25 }}
                source={require("../assets/Logo.png")}
            />
            <TouchableOpacity style={styles.buttonDaftar}>
                <Text style={styles.textDaftar} onPress={()=>navigation.navigate ("Daftar")}>Daftar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonMasuk}>
                <Text style={styles.textMasuk} onPress={()=>navigation.navigate ("Login")}>Masuk</Text>
                </TouchableOpacity>
        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#FF7F50",
      },
      buttonDaftar: {
        height: 50,
        width: 150,
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: "#FDCDBA",
        top: -20,
        left: 20,
    },
    buttonMasuk: {
        height: 50,
        width: 150,
        alignSelf: 'flex-end',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: "#22C7A9",
        top: -70,
        right: 20,
    },
    textMasuk: {
        fontSize: 18,
        color: "black",
    },
    textDaftar: {
        fontSize: 18,
        color: "black",
    }
})