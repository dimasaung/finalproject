import React, { useEffect, useState } from "react";
import { Text, View, Image, Button, FlatList, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import {useNavigation} from "@react-navigation/native"
import { FontAwesome } from '@expo/vector-icons';

import { Data }from './data'
import { Header, HorizontalCafeCard } from "../components";

const Section = ({title, onPress, children}) => {
    return (
        <View>
            {/* {Header} */}
            <View
                style={{
                    flexDirection: 'row',
                    marginHorizontal: 10,
                    marginTop: 30,
                    marginBottom: 20
                }}
            >
                <Text style={{ flex: 1}}>
                    Mungkin Anda Suka
                </Text>

                <TouchableOpacity
                    onPress={{onPress}}
                >
                    <Text style={{color: 'black'}}>
                        Show All
                    </Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default function HomeScreen({navigation}) {
    
    // const [selectedCategoryId, setSelectedCategoryId] = useState(1)
    // const [selectedCafeType, setSelectedCafeType] = useState(1)
    // const [cafeList, setCafeList] = useState([])
    const [kategori, setKategori] = useState ([
        {
            nama : 'Semua'
        },
        {
            nama : 'Cafe'
        },
        {
            nama : 'Restaurant'
        },
        {
            nama : 'Coffe Shop'
        },
    ])

    // useEffect(() => {
    //     handleChangeCategory(selectedCategoryId, selectedCafeType)
    // },[])

    // // Handler
    // function handleChangeCategory(categoryId, CafeTypeId) {
    //     // Cari cafe berdasarkan CafeTypeId
    //     let selectedCafe = Data.title.find(a => a.id == CafeTypeId)

        // Set cafe berdasarkan categoryId
    //     setCafeList(selectedCafe?.list.filter(a => a.categories.includes(categoryId)))
    // }

    // Render
    // RenderSearch
    function renderSearch() {
        return(
           <View
                style={{
                    flexDirection: 'row',
                    height: 45,
                    alignItems: 'center',
                    marginHorizontal: 10,
                    marginVertical: 10,
                    paddingHorizontal: 10,
                    borderRadius: 15,
                    backgroundColor: '#FFF9F3'
                }}
           >
                {/* { Icon Search } */}
                <FontAwesome name="search" size={24} color="black" />


                {/* {Text Input} */}
                <TextInput
              style={{
                top: 5,
                flex: 1,
                paddingVertical: 10,
                borderRadius: 15,
                width: 337,
                height: 45,
                marginBottom: 10,
                paddingHorizontal: 10,
                backgroundColor: '#FFF9F3',
              }}
              placeholder="Search Cafe"
            />
           </View>
           )
        }
    // function renderMenuTypes() {
    //     return (
    //         <FlatList
    //             horizontal
    //             data= {Category, inde}
    //             keyExtractor={item => '${item.id}'}
    //             showHorizontalScrollIndicator={false}
    //             contentContainerStyle={{
    //                 marginTop: 30,
    //                 marginBottom: 20,
    //             }} 
    //             renderItem={({item}) => {
    //                 <TouchableOpacity
    //                     style={{
    //                         marginLeft: 10,
    //                         marginRight: 10,
    //                     }}
    //                 >
    //                     <Text 
    //                         style={{
    //                             fontSize: 50
    //                         }}
    //                     >
    //                     {item.category}
    //                     </Text>
    //                 </TouchableOpacity>
    //             }}
    //         />
    //     )
    // }
    const [kategoriSeleksi, setKategoriSeleksi] = useState({
        nama : 'Cafe'
    })

    const [dataTrending, setDataTrending] = useState([
        { namaCafe: 'Cafe 8', jenis: 'Cafe', image: require("../assets/Cafe8.jpg") },
        { namaCafe: 'Cafe 2', jenis: 'Restaurant', image: require("../assets/Cafe2.jpg") },
        { namaCafe: 'Cafe 5', jenis: 'Coffe Shop', image: require("../assets/Cafe5.jpg") },
        { namaCafe: 'Cafe 1', jenis: 'Kaki Lima', image: require("../assets/Cafe1.jpg")},
    ])
    return (
        <View style={{
                flex: 1,
                backgroundColor : '#FFA07A'
            }}
        >
            {/* { Header } */}
            <Header
                containerStyle={{
                    height: 50,
                    paddingHorizontal: 10,
                    marginTop: 20,
                    marginBottom: 10,
                    alignSelf: 'flex-start'
                }}
                rightComponent={
                    <TouchableOpacity
                        style={{
                            width: 40,
                            height:40,
                            alignItems: 'center',
                            justifyContent: 'center',
                            right:10
                        }}
                        onPress={()=> navigation.navigate("Profil")}
                    >   
                        <Image
                        style={{ height: 55, width: 55, borderRadius: 25}}
                        source={require("../assets/profile.png")}
                        /> 
                    </TouchableOpacity>
                }
            />
            {/* {Search} */}
            {renderSearch()}
            
            

            {/* {Label} */}
            <View
                style={{
                    flexDirection: 'row',
                    marginHorizontal: 20,
                    marginBottom: 10
                }}
            >    
                <Text style={{fontSize:16, top: 5, color: "#FFFFFF"}}>Mungkin Anda Suka </Text>
                
                <TouchableOpacity style={{
                    justifyContent: 'center', alignItems: 'flex-end', flex: 1
                }}>
                <Text style={{fontSize:12, top: 5, color: "grey"}}>Lihat Semua </Text>
                </TouchableOpacity>
            </View>
            
            {/* {Suka} */}
            <View>
            <FlatList
                data={Data}
                horizontal
                showsHorizontalScrollIndicator={false}
                ListHeaderComponent={
                    <View>
                        {/* {Menu Type} */}
                        {/* {renderMenuTypes()} */}
                    </View>
                }
                renderItem={({ item }) => {
                    return (
                            <HorizontalCafeCard
                                containerStyle={{
                                    height: 130,
                                    width: 337,
                                    alignItems: 'center',
                                    marginHorizontal: 10,
                                    marginBottom: 10
                                }}
                                imageStyle={{
                                    marginTop: 20,
                                    height: 110,
                                    width: 110,
                                    left: 15,
                                    top: -10,
                                    borderRadius: 10,
                                }}
                                item={item}
                                onPress={()=> console.log
                                ("HorizontalCafeCard")}
                            />
                    )
                }}
            />
            </View>

            {/* {Kategori} */}
            <View>
                <FlatList
                data={kategori}
                horizontal
                renderItem={({item}) => (
                    <TouchableOpacity
                    style={{
                        marginTop: 10,
                        marginRight: 10,
                        marginBottom: 10,
                        left: 10,
                        backgroundColor: kategoriSeleksi.nama == item.nama ? 'lightblue' : 'white',
                        borderRadius: 15,
                        paddingHorizontal: 12,
                        paddingVertical: 8,
                        elevation: 3,                        
                        }}
                        onPress={() => setKategoriSeleksi(item)}
                    >
                        <Text>{item.nama}</Text>
                    </TouchableOpacity>
                )}
                />
            </View>

            {/* {Content} */}
            <View
                style={{
                    flex:1
            }}
            />
            
            <View>
            <FlatList
                data={Data}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => {
                    return (
                            <HorizontalCafeCard
                                containerStyle={{
                                    height: 130,
                                    width: 337,
                                    alignItems: 'center',
                                    marginHorizontal: 10,
                                    marginBottom: 10
                                }}
                                imageStyle={{
                                    marginTop: 20,
                                    height: 110,
                                    width: 110,
                                    left: 15,
                                    top: -10,
                                    borderRadius: 10,
                                }}
                                item={item}
                                onPress={()=> console.log
                                ("HorizontalCafeCard")}
                            />
                    )
                }}
            />
            </View>                  
        </View>
    )
}