import React, { useEffect, useState } from "react";
import { Text, View, Image, Button, FlatList, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import {useNavigation} from "@react-navigation/native"
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

export default function AddScreen({navigation}) {
    return (
        <View style={{
                flex: 1,
                backgroundColor : '#FFA07A'
            }}
        >
        
            {/* {Profil Saya} */}
            <View
                style={{
                    borderBottomWidth : 1,
                    left: 15,
                    paddingVertical: 10,
                }}
            >
                <Text>Profil Saya</Text>
                <View >
                    <Text style={{paddingVertical: 10}}>Profil</Text>
                    <Text style={{paddingVertical: 10}}>Akun</Text>    
                    <Text style={{paddingVertical: 10}}>Privasi</Text>     
                </View>   
            </View>
            {/* {Pengaturan Aplikasi} */}
            <View
                style={{
                    borderBottomWidth : 1,
                    left: 15,
                    paddingVertical: 10,
                }}
            >
                <Text>Pengaturan Aplikasi</Text>
                <View >
                    <Text style={{paddingVertical: 10}}>Notifikasi</Text>
                    <Text style={{paddingVertical: 10}}>Dompetku</Text>    
                    <Text style={{paddingVertical: 10}}>Bahasa</Text>     
                </View>   
            </View>
    
            {/* {Bantuan} */}
            <View
                style={{
                    borderBottomWidth : 1,
                    left: 15,
                    paddingVertical: 10,
                }}
            >
                <Text>Bantuan</Text>
                <View >
                    <Text style={{paddingVertical: 10}}>Pusat Bantuan</Text>
                    <Text style={{paddingVertical: 10}}>Tentang</Text>    
                </View>   
            </View>
        <View>
            </View>                  
        </View>
    )
}