import {useNavigation} from "@react-navigation/native"
import React, { useState, } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button, TouchableOpacity } from "react-native";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth"
// import { Entypo } from '@expo/vector-icons';

export default function RegisterScreen({navigation}) {  

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("");

    const onSignedUp = () => {
        const auth = getAuth();
        if (confirmPassword===password) {
          createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
              // Masuk
              const user = userCredential.user;
              console.log(`Registered with : ${user.email}`);
              alert('Registered is success !!');
              navigation.navigate('Sukses');
              setEmail("");
              setPassword("");
              setConfirmPassword("");
              })
            .catch((error) => {
              const errorCode = error.code;
              const errorMessage = error.message;
              alert(errorMessage)
              console.log(errorCode, errorMessage);
            });
        } else {
          alert("Password not match !!");
        }
      }
    
    return (
        <View style={styles.container}>
            <Image
                style={{ height: 140, width: 320, top : -40 }}
                source={require("../assets/Daftar.png")}
            />
            <View>
            <TextInput
              style={{
                paddingVertical: 10,
                borderRadius: 20,
                width: 300,
                height: 63,
                marginBottom: 10,
                paddingHorizontal: 10,
                backgroundColor: '#F3F3F3',
              }}
              placeholder="Email"
                value={email}
                onChangeText={(value)=>setEmail(value)} 
            />
            <TextInput
              style={{
                paddingVertical: 10,
                borderRadius: 20,
                width: 300,
                height: 63,
                marginBottom: 10,
                paddingHorizontal: 10,
                backgroundColor: '#F3F3F3',
              }}
              placeholder="Password"
              value={password}
              onChangeText={(value)=>setPassword(value)}
            />
            <TextInput
              style={{
                paddingVertical: 10,
                borderRadius: 20,
                width: 300,
                height: 63,
                marginBottom: 10,
                paddingHorizontal: 10,
                backgroundColor: '#F3F3F3',
              }}
              placeholder="Konfirmasi Password"
              value={confirmPassword}
              onChangeText={(value)=>setConfirmPassword(value)}
            />
            <TouchableOpacity style={styles.buttonDaftar}>
                <Text style={styles.textDaftar} onPress={onSignedUp}>Daftar</Text>
            </TouchableOpacity>        
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#FF7F50",
    },
    buttonDaftar: {
        height: 50,
        width: 150,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: "#22C7A9",
        bottom: -30,
    },
    textDaftar: {
        fontSize: 18,
        color: "black",
    }    
})