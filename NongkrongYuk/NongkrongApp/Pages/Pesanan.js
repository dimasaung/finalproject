import React from "react";
import { StyleSheet, Text, View, Button} from 'react-native'

export default function Pesanan({navigation}) {
    return (
        <View style={styles.container}>
        <Text style ={{fontSize: 35}}>Pesanan</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: "#FF7F50",
    }
})
