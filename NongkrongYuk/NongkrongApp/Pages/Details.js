import React from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList, TouchableOpacity } from 'react-native'
import { Menu }from './Menu'
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';


export default function Details() {
    return (
        <View style={{
            backgroundColor : '#FFA07A',
        }}>
            {/* {Detail Restaurant} */}
            <View>
            <Image
                style={{ height: 120, width: 350, alignSelf: 'center' }}
                source={require("../assets/Cafe1.jpg")}
            />
            <View style={{
                alignSelf : 'flex-start',
                marginBottom : 10
            }} 
            >
                <Text style={{fontSize:16, fontWeight: 'bold', left: 15, color: "#FFFFFF"}}>Cafe 1 </Text>
                <Text style={{fontSize:16, top: 5, left: 15, color: "black"}}>Alamat Cafe 1 </Text>
                <Text style={{fontSize:16, top: 5, left: 15, color: "black"}}>Jam Buka Cafe 1 </Text>
                <Text style={{fontSize:16, top: 5, left: 15, color: "black"}}>Range Harga Cafe 1 </Text>
                <Text style={{fontSize:16, top: 5, left: 15, color: "black"}}>Type Cafe 1 </Text>
                <Text style={{fontSize:16, top: 5, left: 15, color: "black"}}>Jenis Cafe 1 </Text>
            </View>
            </View>

            {/* {Fasilitas}     */}
            <View 
                style ={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    left : 15,
            }}>
            <AntDesign name="wifi" size={24} color="black" />
            <MaterialCommunityIcons name="parking" size={24} color="black" />
            <MaterialCommunityIcons name="power-socket-jp" size={24} color="black" />
            </View>

            {/* {Label} */}
            <Text style={{fontSize:16, fontWeight: 'bold', alignSelf: 'flex-start', left: 15, color: "#FFFFFF"}}>Menu </Text>

            {/* {Menu} */}
        <View
            style={{
                left: 15
            }}>    
        <FlatList
            data={Menu}
            renderItem={({ item }) => (
                <View style={styles.content}>
                    <Text style={styles.Label}>{item.title}</Text> 
                    <Image style={styles.produk} source={item.image} />
                    <Text style={styles.harga}>{item.harga}</Text>
                </View>
            )}
        />
        
        </View>
    </View>
       
    )
}

const styles = StyleSheet.create({
    content:{
        width: 300,
        height: 100,        
        margin: 5,
        borderWidth:1,
        borderRadius: 5,
        borderColor:'grey',    
    },
    produk: {
        width: 130,
        height: 80,
        left: 10,
        top: -10,
        borderRadius: 40,
    },
    Label: {
        fontSize: 14,
        alignSelf: "flex-start",
        top: 20,        
        left: 150,
        fontWeight: "bold"
    },
    harga: {
        fontSize: 12,
        alignSelf: "flex-start",
        top: -55,        
        left: 165,
    }
})
