import React, { useEffect, useState } from "react";
import { Text, View, Image, Button, FlatList, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import {useNavigation} from "@react-navigation/native"
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

export default function AddScreen({navigation}) {
    
        const [kategori, setKategori] = useState ([
        {
            nama : 'Semua'
        },
        {
            nama : 'Voucher Makanan'
        },
        {
            nama : 'Voucher Minuman'
        },
    ])

        function renderSearch() {
        return(
           <View
                style={{
                    flexDirection: 'row',
                    height: 45,
                    alignItems: 'center',
                    marginHorizontal: 10,
                    marginVertical: 10,
                    marginBottom: 50,
                    paddingHorizontal: 10,
                    borderRadius: 15,
                    top: 10,
                    backgroundColor: '#FFF9F3'
                }}
           >
                {/* { Icon Search } */}
                <FontAwesome name="search" size={24} color="black" />

                {/* {Text Input} */}
                <TextInput
                  style={{
                    top: 5,
                    flex: 1,
                    paddingVertical: 10,
                    borderRadius: 15,
                    width: 337,
                    height: 45,
                    marginBottom: 10,
                    paddingHorizontal: 10,
                    backgroundColor: '#FFF9F3',
              }}
              placeholder="Search Voucher"
            />
           </View>
           )
        }
    
    const [kategoriSeleksi, setKategoriSeleksi] = useState({
        nama : 'Cafe'
    })

    const [dataTrending, setDataTrending] = useState([
        { namaCafe: 'Voucher 1', jenis: '1 Mei 2022',  },
        { namaCafe: 'Voucher 2', jenis: '8 Mei 2022',},
        { namaCafe: 'Voucher 3', jenis: '15 Mei 2022', },
        { namaCafe: 'Voucher 4', jenis: '22 Mei 2022', },
        { namaCafe: 'Voucher 4', jenis: '22 Mei 2022', },

    ])
    return (
        <View style={{
                flex: 1,
                backgroundColor : '#FFA07A'
            }}
        >
            {/* {Search} */}
            {renderSearch()}
            
            {/* {Kategori Voucher} */}
            <View>
                <FlatList
                data={kategori}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => (
                    <TouchableOpacity
                    style={{
                        marginTop: 10,
                        marginRight: 10,
                        marginBottom: 10,
                        left: 10,
                        backgroundColor: kategoriSeleksi.nama == item.nama ? 'lightblue' : 'white',
                        borderRadius: 15,
                        paddingHorizontal: 12,
                        paddingVertical: 8,
                        elevation: 3,                        
                        }}>
                        <Text>{item.nama}</Text>
                    </TouchableOpacity>
                )}
                />
            </View>

            <View>
                <FlatList
                data={dataTrending}
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => (
                    <TouchableOpacity
                    style={{
                        marginTop: 10,
                        marginRight:15,
                        marginBottom: 10,
                        left: 10,
                        backgroundColor: 'white',
                        borderRadius: 15,
                        paddingHorizontal: 15,
                        paddingVertical: 8,
                        elevation: 3,                        
                        }}>
                        <FontAwesome5 name="ticket-alt" size={24} color="black" />
                        <Text
                        styles={{
                            fontSize: 20,
                        }}>
                        {item.namaCafe}
                        </Text> 
                        <Text
                        styles={{
                            fontSize: 20, fontWeight: 'bold',
                        }}>{item.jenis}
                        </Text>
                    </TouchableOpacity>
                )}
                />
            </View>

            {/* {Content} */}
            <View
                style={{
                    flex:1
            }}
            />
            <View>
            
            </View>                  
        </View>
    )
}