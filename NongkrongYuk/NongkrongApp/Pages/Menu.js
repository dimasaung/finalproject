const Menu =[
    {
        id : "1",
        harga: "Rp 10000",
        image: require("../assets/Lotek.jpg"),
        title : "Lotek",
        jenis : "Makanan" 
    },
    // {
    //     id : "2",
    //     harga: "Rp 25000",
    //     image: require("../assets/NasiGoreng.jpg"),
    //     title : "Nasi Goreng",
    // },
    // {
    //     id : "3",
    //     harga: "Rp 35000",
    //     image: require("../assets/NasiKebuli.jpg"),
    //     title : "Nasi Kebuli",
    // },
    {
        id : "4",
        harga: "Rp 8000",
        image: require("../assets/NasiKuning.jpg"),
        title : "Nasi Kuning",
        jenis : "Makanan" 
    },
    {
        id : "5",
        harga: "Rp 45000",
        image: require("../assets/Steak.jpg"),
        title : "Steak",
        jenis : "Makanan" 
    },
    {
        id : "6",
        harga: "Rp 25000",
        image: require("../assets/Cappucino.jpg"),
        title : "Cappucino",
        jenis : "Minuman" 
    },
    {
        id : "7",
        harga: "Rp 15000",
        image: require("../assets/LemonTea.jpg"),
        title : "Lemon Tea",
        jenis : "Minuman"  
    },
    {
        id : "8",
        harga: "Rp 5000 - Rp 35000",
        image: require("../assets/MineralWater.jpg"),
        title : "Mineral Water",
        jenis : "Minuman" 
    }
]

export {Menu};