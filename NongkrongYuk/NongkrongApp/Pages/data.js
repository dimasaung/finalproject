const Data =[
    {
        id : "1",
        image : require("../assets/Cafe1.jpg"),
        title : "Cafe 1",
        alamat : "Jl. Ruko 1 no 1",
        jam : "08.00 - 21.00",
        harga : "Rp 10000 - Rp 35000",
        type : "Dine In",
        jenis : "Cafe"
    },
    {
        id : "2",
        image : require("../assets/Cafe2.jpg"),
        title : "Cafe 2",
        alamat : "Jl. Ruko 1 no 3",
        jam : "09.00 - 24.00",
        harga : "Rp 50000 - Rp 150000",
        type : "Dine In / Take Away",
        jenis : "Restaurant"
    },
    {
        id : "3",
        image : require("../assets/Cafe3.jpg"),
        title : "Cafe 3",
        alamat : "Jl. Ruko 1 no 5",
        jam : "08.00 - 22.00",
        harga : "Rp 15000 - Rp 75000",
        type : "Dine In / Delivery",
        jenis : "Coffe Shop"
    },
    {
        id : "4",
        image : require("../assets/Cafe4.jpg"),
        title : "Cafe 4",
        alamat : "Jl. Ruko 2 no 2",
        jam : "10.00 - 21.00",
        harga : "Rp 20000 - Rp 35000",
        type : "Dine in / Take Away / Delivery",
        jenis : "Coffe Shop"
    },
    {
        id : "5",
        image : require("../assets/Cafe5.jpg"),
        title : "Cafe 5",
        alamat : "Jl. Ruko 2 no 4",
        jam : "11.00 - 20.00",
        harga : "Rp 30000 - Rp 60000",
        type : "Dine in / Take Away / Delivery",
        jenis : "Coffe Shop"       
    },
    {
        id : "6",
        image : require("../assets/Cafe6.jpg"),
        title : "Cafe 6",
        alamat : "Jl. Ruko 2 no 6",
        jam : "Jl. Ruko 2 no 4",
        harga : "Rp 25000 - Rp 50000",
        type : "Delivery",
        jenis : "Restaurant"
    },
    {
        id : "7",
        image : require("../assets/Cafe7.jpg"),
        title : "Cafe 7",
        alamat : "Jl. Ruko 2 no 6",
        jam : "10.00 - 19.00",
        harga : "Rp 20000 - Rp 40000",
        type : "Take Away / Delivery",
        jenis : "Cafe"
    },
    {
        id : "8",
        image : require("../assets/Cafe8.jpg"),
        title : "Cafe 8",
        alamat : "Jl. Ruko 3 no 7",
        jam : "11.00 - 18.00",
        harga : "Rp 5000 - Rp 35000",
        type : "Take Away",
        jenis : "Restaurant"
    }
]

export {Data};